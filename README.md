# My Tiny PKI

An openssl frontend for generating CA and server keys without complex procedures and unintuitive command-lines.

## Roadmap

### v1

- import certificates

#### v1.1

- display the subjectAltName in the list
- block form submission if the subjectAltName input is not empty
- check that CN is covered by at least one of the subjectAltName if any

#### v1.2 

- ensure that a certificate can't be created with a lifetime out of its CA's lifetime

### v2

- allow CA without key, so that we can import real certificates for expiry monitoring
- send alert when a certificate is about to expire
- renew an expiring server certificate
- renew an expiring CA and its child certificates
- user profile (update credentials)

### v3 

- check whether the domain names (CN and SANs) exists, and warn if they don't
  - if not, check whether the domain names (CN and SANs) are valid, and warn if they aren't
- user roles
  - allow some users to only download public keys
  - allow unauthenticated users to download public keys (unless protected)
- mutex in certificate creation, so that tests can run in parallel, or temp dirs
