use std::fs;
use std::process::Command;
use crate::san::SubjectAlternativeNames;

pub struct RootCrtGen;
impl RootCrtGen {
    fn generate_openssl_cfg() -> &'static str {
        "[v3_ext]\n\
        subjectKeyIdentifier = hash\n\
        authorityKeyIdentifier = keyid:always,issuer\n\
        basicConstraints = CA:TRUE, pathlen:1\n\
        keyUsage = keyCertSign\n"
    }

    pub fn generate_certificate(crt: &str, csr: &str, key: &str, days: u64) -> String {
        let mut args = CrtGenHelper::get_common_args_vec(crt, csr, days);
        args.extend_from_slice(&vec![
            "-signkey", key,
        ].iter().map(|&s| s.into()).collect::<Vec<String>>());

        CrtGenHelper::run_openssl(args, Self::generate_openssl_cfg(), None);

        fs::read_to_string(crt).expect("Failed to read public key file")
    }
}

pub struct IntermediateCrtGen;
impl IntermediateCrtGen {
    fn generate_openssl_cfg() -> &'static str {
        "[v3_ext]\n\
        subjectKeyIdentifier = hash\n\
        authorityKeyIdentifier = keyid:always,issuer\n\
        basicConstraints = CA:TRUE, pathlen:0\n\
        keyUsage = keyCertSign\n"
    }

    pub fn generate_certificate(crt: &str, csr: &str, signcrt: &str, signkey: &str, days: u64) -> String {
        let mut args = CrtGenHelper::get_common_args_vec(crt, csr, days);
        args.extend_from_slice(&vec![
            "-CA", signcrt,
            "-CAkey", signkey,
        ].iter().map(|&s| s.into()).collect::<Vec<String>>());

        CrtGenHelper::run_openssl(args, Self::generate_openssl_cfg(), Some("01".to_string()));

        fs::read_to_string(crt).expect("Failed to read public key file")
    }
}

pub struct ServerCrtGen;
impl ServerCrtGen {
    fn generate_openssl_cfg(san: &SubjectAlternativeNames) -> String {
        let mut conf = "[v3_ext]\n\
                basicConstraints = CA:false\n\
                nsCertType = server\n\
                subjectKeyIdentifier = hash\n\
                authorityKeyIdentifier = keyid,issuer:always\n\
                keyUsage = digitalSignature, keyEncipherment\n\
                extendedKeyUsage = serverAuth\n".to_string();

        if !san.0.is_empty() {
            conf += &("subjectAltName = ".to_owned() + &san.to_string());
        }

        conf
    }

    pub fn generate_certificate(crt: &str, csr: &str, signcrt: &str, signkey: &str, days: u64, san: &SubjectAlternativeNames) -> String {
        let mut args = CrtGenHelper::get_common_args_vec(crt, csr, days);
        args.extend_from_slice(&vec![
            "-CA", signcrt,
            "-CAkey", signkey,
        ].iter().map(|&s| s.into()).collect::<Vec<String>>());

        CrtGenHelper::run_openssl(args, &Self::generate_openssl_cfg(san), Some("01".to_string()));

        fs::read_to_string(crt).expect("Failed to read public key file")
    }
}

struct CrtGenHelper;
impl CrtGenHelper {
    pub fn get_common_args_vec(crt: &str, csr: &str, days: u64) -> Vec<String> {
        vec![
            "x509",
            "-req",
            "-sha256",
            "-days", &days.to_string(),
            "-extfile", "openssl.cnf",
            "-extensions", "v3_ext",
            "-in", csr,
            "-out", crt,
        ].iter().map(|&s| s.into()).collect()
    }

    pub fn run_openssl(arguments: Vec<String>, config: &str, serial: Option<String>) {
        if serial.is_some() {
            fs::write("ca.srl", &serial.as_ref().unwrap()).expect("Failed to write CA srl file");
        }

        fs::write("openssl.cnf", config).expect("Failed to write CA crt file");

        Command::new("openssl")
            .args(arguments)
            .spawn().expect("Certificate creation failed: cannot spawn openssl command")
            .wait().expect("Certificate creation failed: openssl issued an error");

        fs::remove_file("openssl.cnf").expect("Failed to remove openssl config file");

        if serial.is_some() {
            fs::remove_file("ca.srl").expect("Failed to remove CA srl file");
        }
    }
}