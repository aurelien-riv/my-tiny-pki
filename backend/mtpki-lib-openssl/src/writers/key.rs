use std::{fs, str};
use std::process::Command;

pub struct RsaKeyGen;
impl RsaKeyGen {
    pub fn generate_key(key_filename: &str, bits: i32) {
        Command::new("openssl")
            .arg("genrsa")
            .arg("-out").arg(key_filename)
            .arg(bits.to_string())
            .spawn().expect("Private key creation failed: cannot spawn openssl command")
            .wait().expect("Private key creation failed: openssl issued an error");
    }
}

pub struct DsaKeyGen;
impl DsaKeyGen {
    pub fn generate_key(key_filename: &str, bits: i32) {
        Command::new("openssl")
            .arg("dsaparam")
            .arg("-out").arg("dsaparam.pem")
            .arg(bits.to_string())
            .spawn().expect("DSA param file creation failed: cannot spawn openssl command")
            .wait().expect("DSA param file creation failed: openssl issued an error");

        Command::new("openssl")
            .arg("gendsa")
            .arg("-out").arg(key_filename)
            .arg("dsaparam.pem")
            .spawn().expect("Private key creation failed: cannot spawn openssl command")
            .wait().expect("Private key creation failed: openssl issued an error");

        fs::remove_file("dsaparam.pem").expect("Failed to remove DSA param file");
    }
}

pub struct EcdsaKeyGen;
impl EcdsaKeyGen {
    pub fn generate_key(key_filename: &str, curve_name: &str) {
        if ! Self::get_available_curves().contains(&curve_name.to_string()) {
            panic!("Curve {} is not supported", curve_name);
        }

        Command::new("openssl")
            .arg("ecparam").arg("-genkey")
            .arg("-out").arg(key_filename)
            .arg("-name").arg(curve_name)
            .spawn().expect("Private key creation failed: cannot spawn openssl command")
            .wait().expect("Private key creation failed: openssl issued an error");
    }

    pub fn get_available_curves() -> Vec<String> {
        let output = Command::new("openssl")
            .arg("ecparam").arg("-list_curves")
            .output().expect("Failed to execute openssl ecparam -list_curves");

        str::from_utf8(&output.stdout).unwrap()
            .split('\n')
            .map(|s| s.split(':').next().unwrap().trim().to_string())
            .filter(|s| !s.is_empty())
            .collect()
    }
}