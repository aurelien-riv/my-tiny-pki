use std::process::Command;
use crate::san::SubjectAlternativeNames;

pub struct RootCsrGen;
impl RootCsrGen {
    pub fn generate_csr(subject: &str, key_path: &str, csr_path: &str) {
        CsrGenHelper::run_openssl(CsrGenHelper::get_common_args_vec(subject, key_path, csr_path))
    }
}

pub struct IntermediateCsrGen;
impl IntermediateCsrGen {
    pub fn generate_csr(subject: &str, key_path: &str, csr_path: &str) {
        CsrGenHelper::run_openssl(CsrGenHelper::get_common_args_vec(subject, key_path, csr_path))
    }
}

pub struct ServerCsrGen;
impl ServerCsrGen {
    pub fn generate_csr(subject: &str, key_path: &str, csr_path: &str, subject_alt_names: &SubjectAlternativeNames) {
        let mut args = CsrGenHelper::get_common_args_vec(subject, key_path, csr_path);
        if ! subject_alt_names.0.is_empty() {
            args.extend_from_slice(&vec![
                "-addext".to_string(), format!("subjectAltName = {}", subject_alt_names.to_string())
            ]);
        }
        CsrGenHelper::run_openssl(args);
    }
}

struct CsrGenHelper;
impl CsrGenHelper {
    pub fn get_common_args_vec(subject: &str, key_path: &str, csr_path: &str) -> Vec<String> {
        vec![
            "req",
            "-new",
            "-nodes",
            "-sha256",
            "-utf8",
            "-subj", subject,
            "-key", key_path,
            "-out", csr_path
        ].iter().map(|&s| s.into()).collect()
    }

    pub fn run_openssl(arguments: Vec<String>) {
        Command::new("openssl")
            .args(arguments)
            .spawn().expect("Certificate signing request creation failed: cannot spawn openssl command")
            .wait().expect("Certificate signing request creation failed: openssl issued an error");
    }
}