use std::fs;
use std::io::ErrorKind;

pub struct TmpFile {
    pub path: String
}

impl TmpFile {
    pub fn new(path: &str) -> Self {
        TmpFile {
            path: fs::canonicalize(path).unwrap_or_else(|e| panic!("Failed to get the realpath to {} {}", path, e)).to_str().unwrap().to_owned()
        }
    }

    pub fn new_with_content(path: &str, data: &str) -> Self {
        let this = Self::new(path);
        fs::write(path, data).unwrap_or_else(|e| panic!("Failed to write on {}: {}", path, e));
        this
    }
}

impl Drop for TmpFile {
    fn drop(&mut self) {
        match fs::remove_file(&self.path) {
            Ok(()) => (),
            Err(e) if e.kind() == ErrorKind::NotFound => (),
            Err(e) => panic!("Failed to remove {}: {}", self.path, e)
        }
    }
}