use std::str;
use std::process::Command;
use crate::utils::tmpfile::TmpFile;

pub struct Pkcs12Converter;
impl Pkcs12Converter {
    fn get_common_args(p12_path: &str, crt_path: &str, key_path: Option<&str>, passphrase: Option<&str>) -> Vec<String> {
        let mut args: Vec<String> = vec![
            "pkcs12",
            "-export",
            "-out", p12_path,
            "-in", crt_path,
            "-passout", &format!("pass:{}", passphrase.unwrap_or_default())
        ].iter().map(|&s| s.into()).collect();

        if let Some(kp) = key_path {
            args.push("-inkey".to_owned());
            args.push(kp.to_owned());
        } else {
            args.push("-nokeys".to_owned())
        }

        args
    }

    pub fn pem_to_p12(p12_path: &str, crt_path: &str, key_path: Option<&str>, passphrase: Option<&str>) -> TmpFile {
        let args = Self::get_common_args(p12_path, crt_path, key_path, passphrase);

        Command::new("openssl")
            .args(args)
            .spawn().expect("Pkcs12 export failed: cannot spawn openssl command")
            .wait().expect("Pkcs12 export failed: openssl issued an error");

        TmpFile::new(p12_path)
    }
}