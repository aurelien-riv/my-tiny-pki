use std::str;
use std::process::Command;

pub struct PubkeyExtractor;
impl PubkeyExtractor {
    pub fn extract_from_private_key_file(private_key_path: &str) -> Result<String, String> {
        let output = Command::new("openssl")
            .arg("pkey")
            .arg("-pubout")
            .arg("-in").arg(private_key_path)
            .output().expect("Failed to spawn openssl command");

        if ! output.status.success() {
            return Err(format!("Failed to extract the public key from {}. ", private_key_path));
        }

        Ok(str::from_utf8(&output.stdout).unwrap().trim().to_string())
    }

    pub fn extract_from_public_key_file(public_key_path: &str) -> Result<String, String> {
        let output = Command::new("openssl")
            .arg("x509")
            .arg("-noout")
            .arg("-pubkey")
            .arg("-in").arg(public_key_path)
            .output().expect("Failed to spawn openssl command");

        if ! output.status.success() {
            return Err(format!("Failed to extract the public key from {}", public_key_path));
        }

        Ok(str::from_utf8(&output.stdout).unwrap().trim().to_string())
    }
}