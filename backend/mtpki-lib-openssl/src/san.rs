pub struct SubjectAlternativeNames(pub Vec<String>);

impl ToString for SubjectAlternativeNames {
    fn to_string(&self) -> String {
        self.0.iter()
            .map(|d| "DNS:".to_owned() + d)
            .collect::<Vec<String>>()
            .join(",")
    }
}