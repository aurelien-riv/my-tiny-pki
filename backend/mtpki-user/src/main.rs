use std::env;
use diesel::{delete, ExpressionMethods, insert_into, RunQueryDsl, update};
use diesel::query_dsl::filter_dsl::FilterDsl;
use mtpki_lib::database::get_mtpkidb;
use mtpki_lib::models::NewUser;
use mtpki_lib::schema::users;

fn help() {
    println!("USAGE:");
    println!("mtpki-user help");
    println!("mtpki-user add    <username> <email@address.com>");
    println!("mtpki-user del    <username>");
    println!("mtpki-user passwd <username>");
}

fn get_password_from_cli() -> String {
    let passwd = rpassword::prompt_password("New user's password: ")
        .expect("Cannot get the password");
    let confirm = rpassword::prompt_password("Confirm new user's password: ")
        .expect("Cannot get the password");

    if passwd != confirm {
        panic!("Password and confirmation mismatch");
    }

    passwd
}

fn useradd(username: &str, email: &str) {
    let user = NewUser {
        name: username.to_owned(),
        email: email.to_owned(),
        password: bcrypt::hash(get_password_from_cli(), bcrypt::DEFAULT_COST).unwrap()
    };

    insert_into(users::table)
        .values(&user)
        .execute(&mut get_mtpkidb())
        .unwrap();
}

fn userdel(username: &str) {
    let deleted_rows = delete(users::table.filter(users::name.eq(&username)))
        .execute(&mut get_mtpkidb())
        .unwrap();

    if deleted_rows == 0 {
        panic!("No such user");
    }
}

fn userpasswd(username: &str) {
    let password = bcrypt::hash(get_password_from_cli(), bcrypt::DEFAULT_COST).unwrap();

    let updated_rows = update(users::table.filter(users::name.eq(&username)))
        .set(users::password.eq(password))
        .execute(&mut get_mtpkidb())
        .unwrap();

    if updated_rows == 0 {
        panic!("No such user");
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    match args.get(1).unwrap_or(&"_".to_string()).as_str() {
        "add"    if args.len() == 4 => useradd(&args[2], &args[3]),
        "del"    if args.len() == 3 => userdel(&args[2]),
        "passwd" if args.len() == 3 => userpasswd(&args[2]),
        _ => help()
    }
}