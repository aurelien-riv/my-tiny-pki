CREATE TABLE certificates (
    certificate_id TEXT PRIMARY KEY NOT NULL, -- the subject_hash
    parent_certificate_id TEXT NOT NULL REFERENCES certificates(certificate_id), -- the issuer_hash
    distinguished_name TEXT NOT NULL,
    subject_alternative_names TEXT,
    key_usage TEXT NOT NULL,
    basic_constraints TEXT NOT NULL,
    valid_from DATETIME NOT NULL,
    valid_until DATETIME NOT NULL,
    key TEXT NOT NULL,
    crt TEXT NOT NULL
);
