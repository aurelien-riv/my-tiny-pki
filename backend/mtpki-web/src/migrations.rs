use std::fs;
use std::os::unix::fs::PermissionsExt;
use diesel_migrations::{MigrationHarness, embed_migrations, EmbeddedMigrations};
use rocket::{Rocket, Build};
use mtpki_lib::database::{get_mtpkidb, get_mtpkidb_path};


pub async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("../migrations");
    get_mtpkidb().run_pending_migrations(MIGRATIONS).unwrap();
    secure_db_files();
    rocket
}

fn secure_db_files() {
    let db_filename = get_mtpkidb_path();
    fs::set_permissions(&db_filename, fs::Permissions::from_mode(0o600))
        .unwrap_or_else(|_| panic!("Failed to set permissions 0600 on file {}", &db_filename));
}