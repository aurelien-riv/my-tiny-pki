use std::borrow::Cow;
use std::ffi::OsStr;
use std::path::PathBuf;
use rust_embed::{EmbeddedFile, RustEmbed};
use rocket::http::ContentType;
use rocket::response::content::RawHtml;

#[derive(RustEmbed)]
#[folder = "$CARGO_MANIFEST_DIR/../../frontend/dist"]
struct Asset;

#[get("/")]
pub async fn index<'r>() -> Option<RawHtml<Cow<'static, [u8]>>> {
    let asset = Asset::get("index.html")? as EmbeddedFile;
    Some(RawHtml(asset.data))
}

#[get("/<path..>", rank=1)]
pub async fn files<'r>(path: PathBuf) -> Option<(ContentType, Cow<'static, [u8]>)> {
    let filename = path.display().to_string();
    let asset = Asset::get(&filename)? as EmbeddedFile;
    let content_type: ContentType = path
        .extension()
        .and_then(OsStr::to_str)
        .and_then(ContentType::from_extension)
        .unwrap_or(ContentType::Bytes);

    Some((content_type, asset.data))
}

#[get("/<_..>", rank=2)]
pub async fn fallback_url<'r>() -> Option<RawHtml<Cow<'static, [u8]>>> {
    index().await
}
