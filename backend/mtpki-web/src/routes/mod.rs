mod auth;
mod certificate;

use rocket::routes;
use auth::login::login;
use certificate::get_curves::get_available_curves;
use certificate::delete_certificate::delete_certificate;
use certificate::get_certificates::{get_certificates, get_root_certificates};
use certificate::get_keys::{get_key, get_crt, get_crt_chain};
use certificate::get_pkcs12::get_pkcs12;
use certificate::post_create_root::create_root;
use certificate::post_create_intermediate::create_intermediate;
use certificate::post_create_server::create_server;

pub fn get_api_routes() -> Vec<rocket::Route> {
    routes![
        login,
        get_available_curves,
        delete_certificate,
        get_certificates,
        get_root_certificates,
        get_key,
        get_crt,
        get_crt_chain,
        get_pkcs12,
        create_root,
        create_intermediate,
        create_server
    ]
}