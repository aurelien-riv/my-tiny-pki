use diesel::prelude::*;
use diesel::{QueryDsl, RunQueryDsl};
use rocket::response::status;
use rocket::serde::json::Json;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::Certificate;
use mtpki_lib::schema::certificates;

#[get("/certificates/<parent_certificate_id>")]
pub fn get_certificates(parent_certificate_id: String, _auth: UserToken) -> Result<Json<Vec<Certificate>>, status::Unauthorized<()>> {
    let certs = certificates::table
        .select(certificates::all_columns)
        .filter(certificates::parent_certificate_id.eq(&parent_certificate_id))
        .filter(certificates::certificate_id.ne(&parent_certificate_id))
        .load::<Certificate>(&mut get_mtpkidb())
        .unwrap();

    Ok(Json(certs))
}

#[get("/certificates")]
pub fn get_root_certificates(_auth: UserToken) -> Result<Json<Vec<Certificate>>, status::Unauthorized<()>> {
    let certs = certificates::table
        .select(certificates::all_columns)
        .filter(certificates::parent_certificate_id.eq(certificates::certificate_id))
        .load::<Certificate>(&mut get_mtpkidb())
        .unwrap();

    Ok(Json(certs))
}
