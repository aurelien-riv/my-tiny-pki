use diesel::prelude::*;
use diesel::{delete, QueryDsl, RunQueryDsl};
use rocket::response::status;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::Certificate;
use mtpki_lib::schema::certificates;

#[delete("/certificates/<certificate_id>")]
pub async fn delete_certificate(certificate_id: String, _auth: UserToken) -> Result<status::NoContent, status::Unauthorized<()>> {
    recursive_delete_certificates(&mut get_mtpkidb(), &certificate_id);
    Ok(status::NoContent)
}

fn recursive_delete_certificates(cnx: &mut SqliteConnection, certificate_id: &str) {
    Certificate::load_by_parent_id(cnx, certificate_id)
        .iter()
        .for_each(|c| recursive_delete_certificates(cnx, &c.certificate_id));

    delete(certificates::table.filter(certificates::certificate_id.eq(certificate_id)))
        .execute(cnx)
        .unwrap_or_else(|e| panic!("Failed to delete certificate {}: {}", certificate_id, e));
}