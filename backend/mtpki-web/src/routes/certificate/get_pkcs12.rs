use rocket::fs::NamedFile;
use rocket::response::status;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::Certificate;
use mtpki_lib::services::crtutils::get_certificate_chain;
use mtpki_lib::services::openssl::pkcs12_export::Pkcs12Export;

#[get("/certificates/<crt_id>/pkcs12")]
pub async fn get_pkcs12(crt_id: String, _auth: UserToken) -> Result<NamedFile, status::Unauthorized<()>> {
    let cert = Certificate::load_by_id(&mut get_mtpkidb(), &crt_id).unwrap();

    // for security reasons, we don't also downloading the CA's private key
    // if really needed, an administrator will still be able to get it from the database
    // if cert.basic_constraints.to_lowercase().contains("ca:true") {
    //     return Err(status::Unauthorized::<()>(None));
    // }

    let p12export = Pkcs12Export { passphrase: None };
    let crt_chain = get_certificate_chain(&mut get_mtpkidb(), &crt_id);
    let p12file = p12export.get_pkcs12(&crt_chain, Some(&cert.key));

    Ok(NamedFile::open(&p12file.path).await.expect("Failed to read the p12 file"))
}