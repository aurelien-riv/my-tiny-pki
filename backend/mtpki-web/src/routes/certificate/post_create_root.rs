use diesel::prelude::*;
use diesel::{QueryDsl};
use rocket::response::status;
use rocket::serde::json::Json;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::Certificate;
use mtpki_lib::schema::certificates;
use mtpki_lib::services::openssl::cacertificate::CaCertificateRequest;
use mtpki_lib::services::openssl::certinfo::CertInfo;
use mtpki_lib::services::openssl::hash_extractor::HashExtractor;

#[post("/ca", data="<ca_request>")]
pub fn create_root(_auth: UserToken, ca_request: Json<CaCertificateRequest>)
    -> Result<Json<Vec<Certificate>>, status::Unauthorized<()>>
{
    let mut cnx = get_mtpkidb();

    let (key,crt) = ca_request.create_key_pair();
    let ci = CertInfo::from_pem_data(&crt);

    let subject_hash = HashExtractor::extract_subject_hash(&crt).unwrap();
    Certificate {
        certificate_id: subject_hash.clone(),
        parent_certificate_id: subject_hash,
        distinguished_name: ci.subject,
        key_usage: "keyCertSign, cRLSign".to_owned(),
        basic_constraints: "CA:true".to_owned(),
        subject_alternative_names: None,
        valid_from: ci.valid_from,
        valid_until: ci.valid_until,
        key,
        crt
    }.create(&mut cnx);

    let certs = certificates::table
        .select(certificates::all_columns)
        .filter(certificates::parent_certificate_id.eq(certificates::certificate_id))
        .load::<Certificate>(&mut cnx)
        .unwrap();

    Ok(Json(certs))
}