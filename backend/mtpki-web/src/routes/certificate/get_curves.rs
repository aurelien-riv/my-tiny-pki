use rocket::serde::Serialize;
use rocket::serde::json::Json;
use mtpki_lib_openssl::writers::key::EcdsaKeyGen;

#[derive(Serialize)]
pub struct AvailableCurvesResponse {
    ecdsa: Vec<String>
}
#[get("/certificates/available_curves")]
pub async fn get_available_curves() -> Json<AvailableCurvesResponse> {
    Json(AvailableCurvesResponse {
        ecdsa: EcdsaKeyGen::get_available_curves()
    })
}