use rocket::response::status;
use rocket::serde::json::Json;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::Certificate;
use mtpki_lib::services::openssl::certinfo::CertInfo;
use mtpki_lib::services::openssl::hash_extractor::HashExtractor;
use mtpki_lib::services::openssl::intermediatecertificate::IntermediateCertificateRequest;

#[post("/ca/<ca_id>", data="<intermediate_request>")]
pub fn create_intermediate(_auth: UserToken, ca_id: String, intermediate_request: Json<IntermediateCertificateRequest>)
                         -> Result<Json<Vec<Certificate>>, status::Unauthorized<()>>
{
    let mut cnx = get_mtpkidb();

    let ca = Certificate::load_by_id(&mut cnx, &ca_id).unwrap();
    let (key,crt) = intermediate_request.create_key_pair(&ca.crt, &ca.key);
    let ci = CertInfo::from_pem_data(&crt);

    Certificate {
        certificate_id: HashExtractor::extract_subject_hash(&crt).unwrap(),
        parent_certificate_id: ca_id.clone(),
        distinguished_name: ci.subject,
        subject_alternative_names: None,
        key_usage: "cRLSign, keyCertSign".to_owned(),
        basic_constraints: "CA:true, pathlen=0".to_owned(),
        valid_from: ci.valid_from,
        valid_until: ci.valid_until,
        key,
        crt
    }.create(&mut cnx);

    Ok(Json(Certificate::load_by_parent_id(&mut cnx, &ca_id)))
}