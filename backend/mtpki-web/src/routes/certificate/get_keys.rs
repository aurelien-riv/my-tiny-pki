use rocket::response::status;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::Certificate;
use mtpki_lib::services::crtutils::get_certificate_chain;

#[get("/certificates/<crt_id>/key")]
pub fn get_key(crt_id: String, _auth: UserToken) -> Result<String, status::Unauthorized<()>> {
    let cert = Certificate::load_by_id(&mut get_mtpkidb(), &crt_id).unwrap();

    // for security reasons, we don't also downloading the CA's private key
    // if really needed, an administrator will still be able to get it from the database
    if cert.basic_constraints.to_lowercase().contains("ca:true") {
        return Err(status::Unauthorized::<()>(None));
    }

    Ok(cert.key)
}

#[get("/certificates/<crt_id>/crt")]
pub fn get_crt(crt_id: String, _auth: UserToken) -> Result<String, status::Unauthorized<()>> {
    Ok(Certificate::load_by_id(&mut get_mtpkidb(), &crt_id).unwrap().crt)
}

#[get("/certificates/<crt_id>/crt_chain")]
pub fn get_crt_chain(crt_id: String, _auth: UserToken) -> Result<String, status::Unauthorized<()>> {
    Ok(get_certificate_chain(&mut get_mtpkidb(), &crt_id))
}