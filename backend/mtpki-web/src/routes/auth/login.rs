use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::Deserialize;
use mtpki_lib::database::get_mtpkidb;
use crate::auth::UserToken;
use mtpki_lib::models::User;

#[derive(FromForm, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct LoginForm {
    pub email: String,
    pub password: String,
}

fn validate_password(password: &str, form_password: &str) -> Result<(), &'static str> {
    match bcrypt::verify(&form_password, password) {
        Ok(valid) if valid => Ok(()),
        Ok(_) => Err("Invalid email or password"),
        Err(err) => {
            eprintln!("{}", err);
            Err("Password verification failed")
        }
    }
}

#[post("/auth/login", data = "<form>")]
pub async fn login(form: Json<LoginForm>) -> Result<status::Accepted<Json<String>>, status::Unauthorized<Json<String>>> {
    if let Ok(user) = User::find_active_user_by_email(form.email.clone(), &mut get_mtpkidb()) {
        return match validate_password(&user.password.clone(), &form.password) {
            Ok(_) => {
                let token = UserToken::new(user).generate_token();
                Ok(status::Accepted(Some(Json(token))))
            },
            Err(msg) => Err(status::Unauthorized(Some(Json(msg.into()))))
        }
    } else {
        Err(status::Unauthorized(Some(Json("Invalid email or password".into()))))
    }
}