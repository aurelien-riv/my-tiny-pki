#[macro_use] extern crate rocket;

use rocket::{Build, Rocket};
use rocket::fairing::AdHoc;

mod migrations;
mod react;

#[launch]
fn rocket() -> Rocket<Build> {
    rocket::build()
        .attach(AdHoc::on_ignite("Diesel Migrations", migrations::run_migrations))
        .mount("/", routes![react::index, react::files, react::fallback_url])
        .mount("/api", mtpki_web::routes::get_api_routes())
}