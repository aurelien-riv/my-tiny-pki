use chrono::Utc;
use jsonwebtoken::errors::Result;
use jsonwebtoken::TokenData;
use jsonwebtoken::{Header, Validation};
use jsonwebtoken::{EncodingKey, DecodingKey};
use rocket::http::Status;
use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};
use rocket::response::status;
use rocket::serde::{Serialize, Deserialize, json::{Json}};
use mtpki_lib::models::User;

static ONE_WEEK: i64 = 60 * 60 * 24 * 7; // in seconds

#[derive(Debug, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Response {
    pub message: String
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct UserToken {
    // issued at
    pub iat: i64,
    // expiration
    pub exp: i64,
    // data
    pub user_id: i32
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserToken {
    type Error = status::Custom<Json<Response>>;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        if let Some(auth_header) = request.headers().get_one("Authorization") {
            let auth_header = auth_header.to_string();
            if auth_header.starts_with("Bearer") {
                let token_data = auth_header[6..auth_header.len()].trim();
                let token = UserToken::decode_token(token_data);
                if let Ok(token_data) = token {
                    return Outcome::Success(token_data.claims);
                }
            }
        }

        return Outcome::Failure((
            Status::BadRequest,
            status::Custom(
                Status::Unauthorized,
                Json(Response { message: String::from("No token provided or invalid token") }),
            ),
        ));
    }
}

impl UserToken {
    pub fn new(user: User) -> UserToken {
        let now = Utc::now().timestamp_nanos() / 1_000_000_000; // nanosecond -> second
        UserToken {
            iat: now,
            exp: now + ONE_WEEK,
            user_id: user.user_id
        }
    }

    pub fn generate_token(&self) -> String {
        // FIXME danger, the key should be defined the a config file so that it is not common to all deployments/instances
        jsonwebtoken::encode(&Header::default(), self, &EncodingKey::from_secret(include_bytes!("auth.key"))).unwrap()
    }

    pub fn decode_token(token: &str) -> Result<TokenData<UserToken>> {
        // FIXME danger, the key should be defined the a config file so that it is not common to all deployments/instances
        jsonwebtoken::decode::<UserToken>(&token, &DecodingKey::from_secret(include_bytes!("auth.key")), &Validation::default())
    }
}