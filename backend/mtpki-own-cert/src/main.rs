extern crate hostname;
use std::{env, fs};
use std::path::Path;
use mtpki_lib::database::get_mtpkidb;
use mtpki_lib::models::Certificate;
use mtpki_lib::services::crtutils::get_certificate_chain;
use mtpki_lib::services::openssl::cacertificate::CaCertificateRequest;
use mtpki_lib::services::openssl::servercertificate::ServerCertificateRequest;
use mtpki_lib::services::openssl::privatekey_generator::PrivatekeyGenerator;

fn help() -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    println!("USAGE:");

    println!("mtpki-own-cert help");
    println!("\tShows this message\n");

    println!("mtpki-own-cert gen [--hostname=xxxx]");
    println!("\tGenerates a new self-signed certificate for MTPKI.");
    println!("\t[--hostname=xxx]: Use the provided hostname instead of the one returned by the operating system\n");

    println!("mtpki-own-cert use <certificate_id>");
    println!("\tUses a certificate from the database for MTPKI itself\n");

    println!("You can also edit the Rocket.toml file to use any TLS certificate. In that case, you should refer to Rocket's documentation:");
    println!("\thttps://rocket.rs/v0.5-rc/guide/configuration/#tls\n");

    println!(
        "CAUTION: this tool writes on the default location of the certificates.\
        If you edit the Rocket.toml file for using your own certificate, the change made by a future\
        call of this command won't be taken into account."
    );

    Ok(())
}

fn gen_certificate(args: &[String]) -> Result<(), Box<dyn std::error::Error>> {
    let hostname = if let Some(h) = args.iter().find(|x| x.starts_with("--hostname=")) {
        h["--hostname=".len()..].to_owned()
    } else {
        hostname::get().expect("Failed to get the system hostname").into_string().unwrap()
    };

    let (ca_key, ca_crt) = CaCertificateRequest {
        distinguished_name: "/CN=mtpki-default".to_owned(),
        days: 365,
        algorithm: PrivatekeyGenerator::RSA {bits: 2048 }
    }.create_key_pair();

    let (srv_key, srv_crt) = ServerCertificateRequest {
        distinguished_name: format!("/CN={}", hostname),
        alternative_names: vec![],
        days: 364,
        algorithm: PrivatekeyGenerator::RSA {bits: 4096 }
    }.create_key_pair(&ca_crt, &ca_key);

    if ! Path::new("certs").is_dir() {
        fs::create_dir("certs").unwrap();
    }
    fs::write("certs/mtpki.crt", format!("{}\n\n{}\n", srv_crt, ca_crt))
        .expect("Failed to write the certificate key chain");
    fs::write("certs/mtpki.key", srv_key)
        .expect("Failed to write the certificate chain");

    println!("Certificate installed. Please restart mtpki to start using it");

    Ok(())
}

fn use_certificate(crt_id: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut cnx = get_mtpkidb();
    let certificate = Certificate::load_by_id(&mut cnx, crt_id).unwrap();

    if ! certificate.key_usage.contains("keyEncipherment") {
        return Err("That certificate doesn't have keyEncipherment capability".into())
    }

    if ! Path::new("certs").is_dir() {
        fs::create_dir("certs").unwrap();
    }
    fs::write("certs/mtpki.crt", get_certificate_chain(&mut cnx, crt_id))
        .expect("Failed to write the certificate key chain");
    fs::write("certs/mtpki.key", certificate.key)
        .expect("Failed to write the certificate chain");

    println!("Certificate installed. Please restart mtpki to start using it");

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();

    let curdir = env::current_dir().unwrap();
    let config_dir = if let Ok(x) = env::var("ROCKET_CONFIG") {
        Path::new(&x).parent().unwrap().display().to_string()
    } else {
        curdir.display().to_string()
    };
    println!("{}", config_dir);

    env::set_current_dir(config_dir).unwrap();

    let ret = match args.get(1).unwrap_or(&"_".to_string()).as_str() {
        "use" if args.len() == 3 => use_certificate(&args[2]),
        "gen" => gen_certificate(&args[2..]),
        _ => help()
    };

    env::set_current_dir(curdir).unwrap();

    ret
}