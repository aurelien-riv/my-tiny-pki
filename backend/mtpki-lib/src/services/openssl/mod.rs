pub mod privatekey_generator;
pub mod hash_extractor;
pub mod cacertificate;
pub mod intermediatecertificate;
pub mod servercertificate;
pub mod certinfo;
pub mod pkcs12_export;

#[cfg(test)]
mod tests {
    use chrono::Utc;
    use crate::services::openssl::cacertificate::CaCertificateRequest;
    use crate::services::openssl::certinfo::CertInfo;
    use crate::services::openssl::hash_extractor::HashExtractor;
    use crate::services::openssl::intermediatecertificate::IntermediateCertificateRequest;
    use crate::services::openssl::servercertificate::ServerCertificateRequest;
    use crate::services::openssl::privatekey_generator::PrivatekeyGenerator;

    fn create_ca_and_intermediate() -> (String, String) {
        let ca_req = CaCertificateRequest {
            distinguished_name: "/CN=root".to_owned(),
            days: 365,
            algorithm: PrivatekeyGenerator::DSA { bits: 2048 }
        };
        let (ca_key, ca_crt) = ca_req.create_key_pair();
        assert!(ca_key.contains("END DSA PRIVATE KEY"));
        assert!(ca_crt.contains("END CERTIFICATE"));

        let intermediate_req = IntermediateCertificateRequest {
            distinguished_name: "/CN=intermediate".to_owned(),
            days: 364,
            algorithm: PrivatekeyGenerator::ECDSA { curve: "prime256v1".to_string() }
        };
        let (intermediate_key, intermediate_crt) = intermediate_req.create_key_pair(&ca_crt, &ca_key);
        assert!(intermediate_key.contains("END EC PRIVATE KEY"));
        assert!(intermediate_crt.contains("END CERTIFICATE"));

        (intermediate_crt, intermediate_key)
    }

    #[test]
    fn can_create_ca_intermediate_and_server_certificates() {
        let (intermediate_crt, intermediate_key) = create_ca_and_intermediate();
        let srv_req = ServerCertificateRequest {
            distinguished_name: "/CN=server".to_owned(),
            alternative_names: Vec::new(),
            days: 363,
            algorithm: PrivatekeyGenerator::RSA {bits: 2048 }
        };
        let (srv_key, srv_crt) = srv_req.create_key_pair(&intermediate_crt, &intermediate_key);
        assert!(srv_key.contains("END RSA PRIVATE KEY"));
        assert!(srv_crt.contains("END CERTIFICATE"));
    }

    #[test]
    fn can_create_ca_intermediate_and_server_san_certificates() {
        let (intermediate_crt, intermediate_key) = create_ca_and_intermediate();
        let srv_req = ServerCertificateRequest {
            distinguished_name: "/CN=server".to_owned(),
            alternative_names: vec!["a.b.fr".to_owned(), "*.c.org".to_owned()],
            days: 363,
            algorithm: PrivatekeyGenerator::DSA {bits: 2048 }
        };
        let (srv_key, srv_crt) = srv_req.create_key_pair(&intermediate_crt, &intermediate_key);
        assert!(srv_key.contains("END DSA PRIVATE KEY"));
        assert!(srv_crt.contains("END CERTIFICATE"));
        assert_eq!(
            HashExtractor::extract_subject_hash(&intermediate_crt).unwrap(),
            HashExtractor::extract_issuer_hash(&srv_crt).unwrap(),
            "The server certificate issuer_hash should be equal to the subject_hash of its CA"
        );
    }

    #[test]
    fn can_get_certificate_informations() {
        let ca_req = CaCertificateRequest {
            distinguished_name: "/CN=root".to_owned(),
            days: 365,
            algorithm: PrivatekeyGenerator::ECDSA { curve: "prime256v1".to_string() }
        };
        let (_, ca_crt) = ca_req.create_key_pair();

        let certinfo = CertInfo::from_pem_data(&ca_crt);
        assert_eq!(certinfo.valid_from.signed_duration_since(Utc::now().naive_utc()).num_minutes(), 0);
        assert_eq!(certinfo.valid_until.signed_duration_since(certinfo.valid_from).num_days(), 365);
    }
}