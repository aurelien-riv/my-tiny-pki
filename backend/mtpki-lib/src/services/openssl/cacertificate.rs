use std::{env, fs};
use rocket::serde::{Deserialize};
use mtpki_lib_openssl::writers::crt::RootCrtGen;
use mtpki_lib_openssl::writers::csr::RootCsrGen;
use crate::services::openssl::privatekey_generator::PrivatekeyGenerator;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct CaCertificateRequest {
    pub distinguished_name: String,
    pub days: u64,
    pub algorithm: PrivatekeyGenerator
}
impl CaCertificateRequest {
    pub fn create_key_pair(&self) -> (String, String) {
        let curdir = env::current_dir().unwrap();
        env::set_current_dir(env::temp_dir()).unwrap();

        let key = self.algorithm.generate_key("ca.key");

        RootCsrGen::generate_csr(&self.distinguished_name, "ca.key", "ca.csr");

        let crt = RootCrtGen::generate_certificate("ca.crt", "ca.csr", "ca.key", self.days);

        fs::remove_file("ca.key").expect("Failed to remove private key file");
        fs::remove_file("ca.csr").expect("Failed to remove certificate signing request file");
        fs::remove_file("ca.crt").expect("Failed to remove public key file");

        env::set_current_dir(curdir).unwrap();

        (key, crt)
    }
}