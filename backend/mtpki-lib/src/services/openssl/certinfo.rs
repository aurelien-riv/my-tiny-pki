use std::str;
use std::fs;
use std::process::Command;
use chrono::NaiveDateTime;
use regex::Regex;

#[derive(Debug)]
pub struct CertInfo {
    pub subject: String,
    pub subject_alt_names: Option<Vec<String>>,
    pub valid_from: NaiveDateTime,
    pub valid_until: NaiveDateTime
}
impl CertInfo {
    pub fn from_path(crt_path: &str) -> CertInfo {
        CertInfo::from_raw_cert_info(&RawCertInfo::new_from_path(crt_path))
    }

    pub fn from_pem_data(crt: &str) -> CertInfo {
        CertInfo::from_raw_cert_info(&RawCertInfo::new_from_str(crt))

    }
}
impl CertInfo {
    fn from_raw_cert_info(raw_cert_info: &RawCertInfo) -> CertInfo{
        raw_cert_info.get_subject_alt_names();
        CertInfo {
            subject: raw_cert_info.get("subject").to_string(),
            subject_alt_names: raw_cert_info.get_subject_alt_names(),
            valid_from: CertInfo::parse_openssl_date(raw_cert_info.get("notBefore")),
            valid_until: CertInfo::parse_openssl_date(raw_cert_info.get("notAfter"))
        }
    }

    fn parse_openssl_date(date: &str) -> NaiveDateTime {
        let out = Command::new("date")
            .arg("-d").arg(&date)
            .arg("+%s")
            .output()
            .unwrap_or_else(|_| panic!("Failed to execute date -d \"{}\" '+%s'", date));

        if ! out.status.success() {
            panic!("openssl exited with status {}.\n{}", out.status, str::from_utf8(&out.stderr).unwrap());
        }

        let out = str::from_utf8(&out.stdout).unwrap().trim();
        let timestamp = out.parse::<i64>().unwrap();
        NaiveDateTime::from_timestamp(timestamp, 0)
    }
}

struct RawCertInfo {
    data: String
}
impl RawCertInfo {
    pub fn new_from_path(crt_path: &str) -> RawCertInfo {
        RawCertInfo {
            data: RawCertInfo::get_data(crt_path)
        }
    }

    pub fn new_from_str(crt: &str) -> RawCertInfo {
        if ! crt.contains("-----END CERTIFICATE-----") {
            panic!("Only public key, using the PEM format, are supported");
        }

        fs::write("cert.crt", crt).expect("Failed to write public key file");

        let rci = RawCertInfo::new_from_path("cert.crt");

        fs::remove_file("cert.crt").expect("Failed to remove public key file");

        rci
    }

    pub fn get(&self, field: &str) -> &str {
        let re = Regex::new(&format!("{}=(.*)", field)).unwrap();
        let value = re.captures(&self.data).unwrap();
        value.get(1).unwrap().as_str()
    }

    pub fn get_subject_alt_names(&self) -> Option<Vec<String>> {
        let re = Regex::new("X509v3 Subject Alternative Name: \n {4}(.*)").unwrap();
        re.captures(&self.data).map(|value|
            value.get(1)
                .unwrap()
                .as_str()
                .split(',')
                .map(|x| x.trim().to_string())
                .collect()
        )
    }
}
impl RawCertInfo {
    fn get_data(crt_path: &str) -> String {
        let out = Command::new("openssl")
            .arg("x509")
            .arg("-noout")
            .arg("-dates")
            .arg("-subject")
            .arg("-ext").arg("subjectAltName")
            .arg("-in").arg(crt_path)
            .output().expect("Failed to execute `openssl x509 -text -in cert.crt`");

        if ! out.status.success() {
            panic!("openssl exited with status {}.\n{}", out.status, str::from_utf8(&out.stderr).unwrap());
        }

        str::from_utf8(&out.stdout).unwrap().trim().to_owned()
    }
}