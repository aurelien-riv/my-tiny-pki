use std::{env, fs, str};
use std::process::Command;

pub struct HashExtractor {
}
impl HashExtractor {
    pub fn extract_subject_hash(publickey: &str) -> Result<String, String> {
        HashExtractor::extract_hash(publickey, "-subject_hash")
    }

    pub fn extract_issuer_hash(publickey: &str) -> Result<String, String> {
        HashExtractor::extract_hash(publickey, "-issuer_hash")
    }

    pub fn extract_subject_hash_from_path(publickey_path: &str) -> Result<String, String> {
        HashExtractor::extract_hash_from_path(publickey_path, "-subject_hash")
    }

    pub fn extract_issuer_hash_from_path(publickey_path: &str) -> Result<String, String> {
        HashExtractor::extract_hash_from_path(publickey_path, "-issuer_hash")
    }

    fn extract_hash(publickey: &str, extraction_flag: &str) -> Result<String, String> {
        let curdir = env::current_dir().unwrap();
        env::set_current_dir(env::temp_dir()).unwrap();
        fs::write("tmp.crt", publickey).expect("Failed to write the certificate file");

        let hash = HashExtractor::extract_hash_from_path("tmp.crt", extraction_flag);

        fs::remove_file("tmp.crt").expect("Failed to remove the public key file");
        env::set_current_dir(curdir).unwrap();

        hash
    }

    fn extract_hash_from_path(publickey_path: &str, extraction_flag: &str) -> Result<String, String> {
        let output = Command::new("openssl").arg("x509")
            .arg("-noout").arg(extraction_flag)
            .arg("-in").arg(publickey_path)
            .output().expect("Failed to spawn openssl command");

        if ! output.status.success() {
            return Err(format!("Failed to extract the {} from {}. ", extraction_flag, publickey_path));
        }

        Ok(str::from_utf8(&output.stdout).unwrap().trim().to_string())
    }
}