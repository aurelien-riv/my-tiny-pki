use std::{env, fs};
use rocket::serde::{Deserialize};
use mtpki_lib_openssl::writers::crt::IntermediateCrtGen;
use mtpki_lib_openssl::writers::csr::IntermediateCsrGen;
use crate::services::openssl::privatekey_generator::PrivatekeyGenerator;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct IntermediateCertificateRequest {
    pub distinguished_name: String,
    pub days: u64,
    pub algorithm: PrivatekeyGenerator
}
impl IntermediateCertificateRequest {
    pub fn create_key_pair(&self, ca_cert: &str, ca_key: &str) -> (String, String) {
        let curdir = env::current_dir().unwrap();
        env::set_current_dir(env::temp_dir()).unwrap();

        fs::write("ca.key", ca_key).expect("Failed to write CA key file");
        fs::write("ca.crt", ca_cert).expect("Failed to write CA crt file");

        let key = self.algorithm.generate_key("intermediate.key");
        IntermediateCsrGen::generate_csr(&self.distinguished_name, "intermediate.key", "intermediate.csr");
        let crt = IntermediateCrtGen::generate_certificate("intermediate.crt", "intermediate.csr", "ca.crt","ca.key", self.days);

        fs::remove_file("ca.key").expect("Failed to remove CA private key file");
        fs::remove_file("ca.crt").expect("Failed to remove CA private key file");
        fs::remove_file("intermediate.key").expect("Failed to remove intermediate private key file");
        fs::remove_file("intermediate.csr").expect("Failed to remove intermediate private key file");
        fs::remove_file("intermediate.crt").expect("Failed to remove intermediate public key file");

        env::set_current_dir(curdir).unwrap();

        (key, crt)
    }
}