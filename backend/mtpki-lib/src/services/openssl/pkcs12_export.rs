use std::{env, fs};
use mtpki_lib_openssl::converters::pkcs12::Pkcs12Converter;
use mtpki_lib_openssl::utils::tmpfile::TmpFile;

pub struct Pkcs12Export {
    pub passphrase: Option<String>
}
impl Pkcs12Export {
    pub fn get_pkcs12(&self, crt_chain: &str, key: Option<&str>) -> TmpFile {
        let curdir = env::current_dir().unwrap();
        env::set_current_dir(env::temp_dir()).unwrap();

        fs::write("tmp.crt", crt_chain).expect("Failed to write the crt chain file");

        let p12 = match key {
            Some(pkey) => {
                fs::write("tmp.key", pkey).expect("Failed to write the key file");
                let p12 = Pkcs12Converter::pem_to_p12("tmp.p12", "tmp.crt", Some("tmp.key"), self.passphrase.as_deref());
                fs::remove_file("tmp.key").expect("Failed to remove the private key file");
                p12
            }
            None => Pkcs12Converter::pem_to_p12("tmp.p12", "tmp.crt", None, self.passphrase.as_deref())
        };

        fs::remove_file("tmp.crt").expect("Failed to remove the public key file");

        env::set_current_dir(curdir).unwrap();

        p12
    }
}