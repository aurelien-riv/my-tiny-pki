use std::{env, fs};
use rocket::serde::{Deserialize};
use mtpki_lib_openssl::san::SubjectAlternativeNames;
use mtpki_lib_openssl::writers::crt::ServerCrtGen;
use mtpki_lib_openssl::writers::csr::ServerCsrGen;
use crate::services::openssl::privatekey_generator::PrivatekeyGenerator;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct ServerCertificateRequest {
    pub distinguished_name: String,
    pub alternative_names: Vec<String>,
    pub days: u64,
    pub algorithm: PrivatekeyGenerator
}
impl ServerCertificateRequest {
    pub fn create_key_pair(&self, sign_crt: &str, sign_key: &str) -> (String, String) {
        let curdir = env::current_dir().unwrap();
        env::set_current_dir(env::temp_dir()).unwrap();

        fs::write("ca.key", sign_key).expect("Failed to write CA key file");
        fs::write("ca.crt", sign_crt).expect("Failed to write CA crt file");

        let key = self.algorithm.generate_key("server.key");

        ServerCsrGen::generate_csr(
            &self.distinguished_name, "server.key", "server.csr", &SubjectAlternativeNames(self.alternative_names.clone())
        );

        let crt = ServerCrtGen::generate_certificate(
            "server.crt",
            "server.csr",
            "ca.crt",
            "ca.key",
            self.days,
            &SubjectAlternativeNames(self.alternative_names.clone())
        );

        fs::remove_file("ca.key").expect("Failed to remove CA private key file");
        fs::remove_file("ca.crt").expect("Failed to remove CA private key file");
        fs::remove_file("server.key").expect("Failed to remove server private key file");
        fs::remove_file("server.csr").expect("Failed to remove server public key file");
        fs::remove_file("server.crt").expect("Failed to remove server public key file");

        env::set_current_dir(curdir).unwrap();

        (key, crt)
    }
}
