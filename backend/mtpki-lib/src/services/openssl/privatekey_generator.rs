use std::fs;
use rocket::serde::{Deserialize};
use mtpki_lib_openssl::writers::key::{DsaKeyGen, EcdsaKeyGen, RsaKeyGen};

#[derive(PartialEq, Deserialize, Debug)]
pub enum PrivatekeyGenerator {
    RSA { bits: i32 },
    DSA { bits: i32 },
    ECDSA { curve: String },
}
impl PrivatekeyGenerator {
    pub fn generate_key(&self, key_filename: &str) -> String {
        match self {
            PrivatekeyGenerator::RSA { bits } => RsaKeyGen::generate_key(key_filename, *bits),
            PrivatekeyGenerator::DSA { bits } => DsaKeyGen::generate_key(key_filename, *bits),
            PrivatekeyGenerator::ECDSA { curve } => EcdsaKeyGen::generate_key(key_filename, curve)
        }
        fs::read_to_string(key_filename).expect("Failed to read private key file")
    }
}