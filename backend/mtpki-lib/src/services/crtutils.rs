use regex::Regex;
use crate::models::Certificate;

pub fn get_common_name(distinguished_name: &str) -> &str {
    let re = Regex::new(r"/CN=([a-zA-Z0-9.]+)$").unwrap();
    return re.captures(distinguished_name).unwrap().get(1).unwrap().as_str();
}

pub fn get_certificate_chain(cnx: &mut diesel::SqliteConnection, crt_id: &str) -> String {
    let mut cert = Certificate::load_by_id(cnx, crt_id).unwrap();
    let mut output = cert.crt + "\n";

    while cert.certificate_id != cert.parent_certificate_id {
        cert = Certificate::load_by_id(cnx, &cert.parent_certificate_id).unwrap();
        output = output + "\n" + &cert.crt + "\n";
    }

    output
}