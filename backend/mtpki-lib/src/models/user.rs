use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
use rocket::serde::{Serialize, Deserialize};
use crate::schema::*;

#[derive(Debug, Clone, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct User {
    pub user_id: i32,
    pub name: String,
    pub email: String,
    #[serde(skip_serializing)]
    pub password: String,
}
impl User {
    pub fn find_by_id(id: i32, cnx: &mut diesel::SqliteConnection) -> Result<User, diesel::result::Error> {
        users::table.filter(users::user_id.eq(&id)).first::<User>(cnx)
    }

    pub fn find_active_user_by_email(email: String, cnx: &mut diesel::SqliteConnection) -> Result<User, diesel::result::Error> {
        users::table
            .filter(users::email.eq(&email))
            .first::<User>(cnx)
    }
}

#[derive(Serialize, Deserialize, Insertable)]
#[diesel(table_name = users)]
#[serde(crate = "rocket::serde")]
pub struct NewUser {
    pub name: String,
    pub email: String,
    pub password: String
}