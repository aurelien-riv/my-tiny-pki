use chrono::NaiveDateTime;
use diesel::{ExpressionMethods, QueryDsl, QueryResult, RunQueryDsl, insert_into};
use rocket::serde::{Serialize, Deserialize};
use crate::schema::*;

#[derive(Debug, Clone, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct Certificate {
    pub certificate_id: String, // subject_hash
    pub parent_certificate_id: String, // issuer_hash
    pub distinguished_name: String,
    pub subject_alternative_names: Option<String>,
    pub key_usage: String,
    pub basic_constraints: String,
    pub valid_from: NaiveDateTime,
    pub valid_until: NaiveDateTime,
    #[serde(skip_serializing)]
    pub key: String,
    #[serde(skip_serializing)]
    pub crt: String
}
impl Certificate {
    pub fn create(&self, cnx: &mut diesel::SqliteConnection) {
        insert_into(certificates::table)
            .values(self)
            .execute(cnx)
            .unwrap();
    }

    pub fn load_by_id(cnx: &mut diesel::SqliteConnection, crt_id: &str) -> QueryResult<Certificate>  {
        certificates::table
            .select(certificates::all_columns)
            .filter(certificates::certificate_id.eq(crt_id))
            .first::<Certificate>(cnx)
    }

    pub fn load_by_parent_id(cnx: &mut diesel::SqliteConnection, crt_id: &str) -> Vec<Certificate> {
        certificates::table
            .select(certificates::all_columns)
            .filter(certificates::parent_certificate_id.eq(crt_id))
            .filter(certificates::certificate_id.ne(crt_id)) // self signed: certificate_id = parent_certificate_id
            .load::<Certificate>(cnx)
            .unwrap()
    }
}