use rocket::config::Config;
use serde::Deserialize;
use diesel::{Connection, SqliteConnection};

pub type MtpkiDb = SqliteConnection;

#[derive(Deserialize)]
struct CnxDbSettings {
    url: String
}

pub fn get_mtpkidb_path() -> String {
    if let Ok(config) = Config::figment().focus("databases.db").extract::<CnxDbSettings>() {
        config.url
    } else {
        panic!("No database url given, or Rocket.toml file not found");
    }
}

pub fn get_mtpkidb() -> MtpkiDb {
    SqliteConnection::establish(&get_mtpkidb_path()).unwrap()
}
