table! {
    certificates (certificate_id) {
        certificate_id -> Text,
        parent_certificate_id -> Text,
        distinguished_name -> Text,
        subject_alternative_names -> Nullable<Text>,
        key_usage -> Text,
        basic_constraints -> Text,
        valid_from -> Timestamp,
        valid_until -> Timestamp,
        key -> Text,
        crt -> Text,
    }
}

table! {
    users (user_id) {
        user_id -> Integer,
        name -> Text,
        email -> Text,
        password -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    certificates,
    users,
);
