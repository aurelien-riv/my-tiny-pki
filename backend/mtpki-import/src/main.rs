use std::{env,fs};
use std::error::Error;
use mtpki_lib::database::get_mtpkidb;
use mtpki_lib::models::Certificate;
use mtpki_lib::services::openssl::certinfo::CertInfo;
use mtpki_lib::services::openssl::hash_extractor::HashExtractor;
use mtpki_lib_openssl::readers::pubkey_extractor::PubkeyExtractor;

fn help() -> Result<(), Box<dyn Error>> {
    println!("USAGE:");

    println!("mtpki-import help");
    println!("\tShows this message\n");

    println!("mtpki-import <pubkey_path> <secretkey_path>");
    println!("\tImport the (pubkey_path, secretkey_path) key pair. Only pem files are supported for now");

    Ok(())
}

fn import_key_pair(pubkey_path: &str, secretkey_path: &str) -> Result<(), Box<dyn Error>> {
    let pkey_from_crt = PubkeyExtractor::extract_from_public_key_file(pubkey_path)?;
    let pkey_from_key = PubkeyExtractor::extract_from_private_key_file(secretkey_path)?;

    if pkey_from_key != pkey_from_crt {
        return Err(
            "The public and private key file don't match, can't be read, are not respectively \
            the public (certificate) and private key file, or are corrupted.".into()
        )
    }

    let mut cnx = get_mtpkidb();
    let issuer_hash = HashExtractor::extract_issuer_hash_from_path(pubkey_path).unwrap();
    if let Ok(issuer) = Certificate::load_by_id(&mut cnx, &issuer_hash) {
        let ci = CertInfo::from_path(pubkey_path);

        Certificate {
            certificate_id: HashExtractor::extract_subject_hash_from_path(pubkey_path).unwrap(),
            parent_certificate_id: issuer.certificate_id,
            distinguished_name: ci.subject,
            key_usage: "digitalSignature, keyEncipherment".to_owned(), // FIXME
            basic_constraints: "CA:false".to_owned(), // FIXME
            subject_alternative_names: ci.subject_alt_names.map(|v| v.join(", ")),
            valid_from: ci.valid_from,
            valid_until: ci.valid_until,
            key: fs::read_to_string(secretkey_path).expect("Failed to read public key file"),
            crt: fs::read_to_string(pubkey_path).expect("Failed to read public key file")
        }.create(&mut cnx);

        Ok(())
    } else {
        return Err(
            format!("That certificate has the issuer_hash {}, but no certificate with that subject_hash exists in the database. \
            Please, import the certificate that was used to sign that one first.", issuer_hash).into()
        )
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    if args.len() == 3 {
        import_key_pair(args.get(1).unwrap(), args.get(2).unwrap())
    } else {
        help()
    }
}