import React, {MouseEvent} from "react";
import {IconButton, ListItemIcon, ListItemText, Menu, MenuItem} from "@mui/material";
import MoreVertIcon from '@mui/icons-material/MoreVert';

export type MenuButtonItem = {
    icon: React.ReactNode,
    text: string,
    action: () => void
}

export class MenuButton extends React.Component<{items: MenuButtonItem[]}, {anchorEl: Element|undefined}> {
    state = {
        anchorEl: undefined
    }

    onOpen(event: MouseEvent) {
        this.setState({
            anchorEl: event.currentTarget
        });
    }

    onClose() {
        this.setState({
            anchorEl: undefined
        });
    }

    onClickItem(item: MenuButtonItem) {
        this.onClose();
        item.action();
    }

    render() {
        return <React.Fragment>
            <IconButton onClick={this.onOpen.bind(this)}><MoreVertIcon /></IconButton>
            <Menu
                id="long-menu"
                anchorEl={this.state.anchorEl}
                open={this.state.anchorEl !== undefined}
                onClose={this.onClose.bind(this)}
            >
                {this.props.items.map((item, index) =>
                    <MenuItem key={index} onClick={this.onClickItem.bind(this, item)}>
                        <ListItemIcon>{item.icon}</ListItemIcon>
                        <ListItemText>{item.text}</ListItemText>
                    </MenuItem>
                )}
            </Menu>
        </React.Fragment>;
    }
}