import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton
} from "@mui/material";
import React from "react";
import { BackendApi } from "../../../service/BackendApi";

export class KeyDownloader extends React.Component<{
    crt_id: string,
    icon: React.ReactNode,
    key_name: 'private key' | 'public key' | 'public key chain'
}, {
    open: boolean,
    key?: string
}> {
    state = {
        open: false,
        key: undefined
    }

    get keyType() {
        switch (this.props.key_name) {
            case 'private key': return 'key';
            case 'public key': return 'crt';
            case 'public key chain': return 'crt_chain';
        }
    }

    async showKey() {
        const payload = await BackendApi.fetch(`/api/certificates/${this.props.crt_id}/${this.keyType}/`);
        this.setState({
            open: true,
            key: await payload.text()
        });
    }

    render() {
        return <React.Fragment>
            <IconButton title={`Download the ${this.props.key_name}`} onClick={this.showKey.bind(this)}>
                {this.props.icon}
            </IconButton>
            <Dialog maxWidth="md"
                    fullWidth
                    open={this.state.open}
                    onClose={() => this.setState({open: false})}>
                <DialogTitle>{this.props.key_name}</DialogTitle>
                <DialogContent sx={{"font-family": "monospace", "white-space": "break-spaces"}}>
                    {this.state.key}
                </DialogContent>
                <DialogActions>
                    {this.props.key_name === 'public key chain' && <Button onClick={() => this.pkcs12Download()}>Pkcs12 (public key only)</Button>}
                    <Button onClick={() => this.setState({open: false})}>Close</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>;
    }

    async pkcs12Download() {
        const response = await BackendApi.fetch(`/api/certificates/${this.props.crt_id}/pkcs12`)
        const blob = await response.blob();
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(new Blob([blob]));
        link.setAttribute('download', `${this.props.crt_id}.p12`);
        console.log(link.href);
        document.body.appendChild(link);
        link.click();
        link.parentNode!.removeChild(link);
    }
}