import React from "react";
import {
    Collapse,
    IconButton,
    LinearProgress,
    List,
    ListItemButton,
    ListItemText
} from "@mui/material";
import {ExpandLess, ExpandMore} from "@mui/icons-material";
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import LinkIcon from '@mui/icons-material/Link';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import {CertificateRole, CreateCertificateFormDialog} from "./CreateCertificateForm";
import {MenuButton, MenuButtonItem} from "../../../common/MenuButton";
import {CaCertificate} from "./_CaCertificateType";
import {KeyDownloader} from "./KeyDownloader";
import {ExpiryProgress} from "./ExpiryProgress";
import { BackendApi } from "../../../service/BackendApi";

type CertificateEntryProps = Readonly<{
    certificate: CaCertificate,
    onDelete: () => void
}>;
type CertificateEntryState = {
    open: boolean,
    loading: boolean,
    children: CaCertificate[] | undefined,
    displayCreationForm: boolean
};
export class CertificateEntry extends React.Component<CertificateEntryProps, CertificateEntryState> {
    state: CertificateEntryState = {
        open: false,
        loading: false,
        children: undefined,
        displayCreationForm: false
    }

    async onCreateChildCertificate(role: CertificateRole, formdata: FormData) {
        const body = {
            distinguished_name: formdata.get("distinguished_name"),
            days: parseInt(formdata.get("days") as string),
            alternative_names: formdata.getAll("alternative_names[]"),
            algorithm: {
                [formdata.get("signature_algorithm") as string]: {
                    bits: parseInt(formdata.get("bits") as string),
                    curve: formdata.get("curve")
                }
            }
        };
        const payload = await BackendApi.fetchJson(`/api/${role}/${this.props.certificate.certificate_id}`, {
            method: 'POST',
            body: JSON.stringify(body)
        });
        this.setState({
            children: payload,
            open: true,
            displayCreationForm: false
        });
    }

    get contextMenu(): MenuButtonItem[] {
        if (this.props.certificate.key_usage.includes("keyCertSign")) {
            return [
                {icon: <AddIcon />, text: 'Create a new certificate', action: () => this.setState({displayCreationForm: true})},
                {icon: <DeleteIcon />, text: 'Delete', action: this.deleteSelf.bind(this)}
            ];
        } else {
            return [{icon: <DeleteIcon />, text: 'Delete', action: this.deleteSelf.bind(this)}];
        }
    }

    deleteSelf() {
        BackendApi.fetch(`/api/certificates/${this.props.certificate.certificate_id}`, {method: 'DELETE'})
            .then(() => this.props.onDelete());
    }

    render() {
        const no_child = this.state.children !== undefined && this.state.children.length === 0;

        const crt = this.props.certificate;
        const canDownloadKeys = crt.key_usage.includes("digitalSignature") || crt.key_usage.includes("keyEncipherment");
        const canHaveChild = crt.key_usage.includes("keyCertSign");

        return <React.Fragment>
            <ListItemButton>
                <ListItemText
                primary={crt.distinguished_name}
                secondary={
                    <ExpiryProgress valid_from={new Date(this.props.certificate.valid_from)}
                                    valid_until={new Date(this.props.certificate.valid_until)}
                    />
                }
            />
                {canDownloadKeys && <KeyDownloader key_name={"private key"} icon={<VpnKeyIcon/>} crt_id={crt.certificate_id}/>}
                {crt.parent_certificate_id && <KeyDownloader key_name={"public key chain"} icon={<LinkIcon/>} crt_id={crt.certificate_id}/>}
                <KeyDownloader key_name={"public key"} icon={<VerifiedUserIcon/>} crt_id={crt.certificate_id}/>

                <MenuButton items={this.contextMenu}/>

                <CreateCertificateFormDialog
                    open={this.state.displayCreationForm}
                    onSubmit={this.onCreateChildCertificate.bind(this)}
                    onCancel={() => this.setState({displayCreationForm: false})}
                    can_create_server={true}
                    can_create_ca={! crt.basic_constraints.includes("pathlen=0")}
                />

                <IconButton title="View certificates signed with that certificate"
                            disabled={no_child || ! canHaveChild}
                            onClick={this.onOpenClose.bind(this)}
                >
                    {(this.state.open ? <ExpandLess/> : <ExpandMore/>)}
                </IconButton>
            </ListItemButton>
            <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding sx={{ pl: 4 }}>
                    {(this.state.children || []).map(c =>
                        <CertificateEntry
                            key={c.certificate_id}
                            certificate={c}
                            onDelete={this.onChildDeleted.bind(this, c.certificate_id)}
                        />
                    )}
                    {(! this.state.children && this.state.loading) && <LinearProgress />}
                </List>
            </Collapse>
        </React.Fragment>;
    }

    onChildDeleted(certificate_id: string) {
        this.setState({
            children: this.state.children?.filter(c => c.certificate_id !== certificate_id)
        })
    }

    async onOpenClose() {
        if (this.state.open) {
            this.setState({
                open: false
            });
        } else if (this.state.children || this.state.loading) {
            this.setState({
                open: true,
            });
        } else {
            this.setState({
                open: true,
                loading: true
            });

            const children = await BackendApi.fetchJson(`/api/certificates/${this.props.certificate.certificate_id}`);
            this.setState({
                children,
                loading: false
            });
        }
    }
}