import React from "react";
import {
    Card,
    CardActions,
    CardContent, CardHeader,
    IconButton, InputAdornment,
    List,
    ListItem,
    ListItemText,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';

class SubjectCaFormPopover extends React.Component<{
    children: React.ReactElement<any, any>
},{}> {
    render() {
        return <Tooltip placement="right-start" title={<React.Fragment>
            <Typography variant="body2" sx={{ px: 1, pt: 1 }}>Country Name (2 letters)</Typography>
            <Typography variant="body2" sx={{ px: 1 }}>State or Province Name (full name)</Typography>
            <Typography variant="body2" sx={{ px: 1 }}>Locality Name (eg, city)</Typography>
            <Typography variant="body2" sx={{ px: 1 }}>Organization Name (eg, company)</Typography>
            <Typography variant="body2" sx={{ px: 1 }}>Organizational Unit Name (eg, section)</Typography>
            <Typography variant="body2" sx={{ px: 1, pb: 1 }}>Common Name (eg, server FQDN or your name)</Typography>
        </React.Fragment>}>{this.props.children}</Tooltip>;
    }
}

export class ValidityFormPopover extends React.Component<{
    children: React.ReactElement<any, any>
},{}> {
    render() {
        return <Tooltip placement="right-start" title={<React.Fragment>
            <Typography variant="body2" sx={{ px: 1, pt: 1 }}>1 year: 365</Typography>
            {[2,3,4,5,6,7,8].map(y => <Typography key={y} variant="body2" sx={{ px: 1 }}>{y} years: {365*y}</Typography>)}
            <Typography variant="body2" sx={{ px: 1, pb: 1 }}>9 years: {365*9}</Typography>
        </React.Fragment>}>{this.props.children}</Tooltip>;
    }
}

export class DistinguishedNameField extends React.Component {
    render() {
        return <SubjectCaFormPopover>
            <TextField name="distinguished_name"
                       label="Distinguished Name"
                       fullWidth
                       margin="normal"
                       required
                       defaultValue="/C=/ST=/L=/O=/CN="
                       inputProps={{ pattern: '^(/C=.{2})?(/ST=[^/]+)?(/L=[^/]+)?(/O=[^/]+)?/CN=[a-zA-Z0-9 \._-]+$' }}
            />
        </SubjectCaFormPopover>
    }
}

export class ValidityField extends React.Component {
    render() {
        return <ValidityFormPopover>
            <TextField
                fullWidth
                margin="normal"
                label="Validity (days)"
                name="days"
                type="number"
                defaultValue={365}
            />
        </ValidityFormPopover>;
    }
}

export class SANField extends React.Component<{}, {san_input_val: string, domains: string[]}> {
    state = {
        san_input_val: "",
        domains: [] as string[]
    }

    onAddDomain() {
        this.setState({
            domains: [...this.state.domains, this.state.san_input_val],
            san_input_val: ""
        });
    }

    onDelDomain(idx: number) {
        const domains = [...this.state.domains]
        domains.splice(idx, 1);
        this.setState({domains});
    }

    render() {
        return <Card>
            <CardHeader title={
                <Typography sx={{ fontSize: "1.1rem", color: "rgba(0, 0, 0, 0.6)" }}>Suject Alternative Names (SAN)</Typography>
            }/>
            <CardContent sx={{ padding: 0 }}>
                <List component="div" disablePadding dense>
                    {this.state.domains.map((domain, idx) =>
                        <ListItem key={idx} secondaryAction={
                            <IconButton edge="end" onClick={this.onDelDomain.bind(this, idx)}>
                                <DeleteIcon />
                            </IconButton>
                        }>
                            <ListItemText primary={domain}/>
                            <input type="hidden" name="alternative_names[]" value={domain}/>
                        </ListItem>
                    )}
                </List>
            </CardContent>
            <CardActions disableSpacing>
                <TextField fullWidth
                           margin="normal"
                           label="Add another domain"
                           placeholder="bar.foo.com"
                           value={this.state.san_input_val}
                           onChange={evt => this.setState({san_input_val: evt.target.value})}
                           InputProps={{
                               endAdornment: <InputAdornment position="end">
                                   <IconButton title="Add" color="success" onClick={this.onAddDomain.bind(this)}>
                                       <AddIcon />
                                   </IconButton>
                               </InputAdornment>
                           }}
                />
            </CardActions>
        </Card>;
    }
}