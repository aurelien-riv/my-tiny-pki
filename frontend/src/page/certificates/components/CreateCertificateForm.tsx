import React, {FormEvent} from "react";
import {Button, ToggleButton, ToggleButtonGroup, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Stepper, Step, StepLabel, FormControlLabel, TextField, InputLabel, FormControl, Select, MenuItem} from "@mui/material";
import {DistinguishedNameField, SANField, ValidityField} from "./SslFormHelpers";
import { supportedCurves } from "../../../service/supported-curves";

export type CertificateRole = 'ca' | 'server';
export type SignatureAlgorithm = 'RSA' | 'DSA' | 'ECDSA';

type CreateCertificateFormDialogProps = {
    open: boolean,
    can_create_ca: boolean,
    can_create_server: boolean,
    onSubmit: (role: CertificateRole, data: FormData) => void,
    onCancel: () => void
};

export class CreateCertificateFormDialog extends React.Component<CreateCertificateFormDialogProps, {
    role: CertificateRole,
    signatureAlgorithm: SignatureAlgorithm,
    activeStep: number
}> {
    curves: Record<string, string[]>|null;

    state = {
        role: 'ca' as CertificateRole,
        signatureAlgorithm: 'RSA' as SignatureAlgorithm,
        activeStep: 0
    };

    constructor(props: CreateCertificateFormDialogProps) {
        super(props);
        this.curves = null;
        supportedCurves.curves.then(curves => this.curves = curves);
    }

    componentWillReceiveProps(nextProps: CreateCertificateFormDialogProps) {
        if (! this.props.open && !!nextProps.open) {
            this.setState({
                role: (nextProps.can_create_server ? 'server' : 'ca') as CertificateRole,
                activeStep: 0
            })
        }
    }

    render() {
        const lastStep = this.state.role == 'server' ? 2 : 1;

        return <Dialog maxWidth="md"
                       fullWidth
                       open={this.props.open}
                       onClose={() => this.props.onCancel()}>
            <DialogTitle>Create a new certificate</DialogTitle>

            <DialogContent>
                <Stepper activeStep={this.state.activeStep} alternativeLabel>
                    <Step><StepLabel>Type</StepLabel></Step>
                    <Step><StepLabel>Attributes</StepLabel></Step>
                    {this.state.role == 'server' && <Step><StepLabel>Alternative names</StepLabel></Step>}
                </Stepper>
                
                <form id="ca.create" onSubmit={this.onSubmit.bind(this)}>
                    <Grid container style={{display: this.state.activeStep === 0 ? "flex" : "none"}}>
                        <Grid item xs={12} textAlign="center" style={{margin: "1rem 0"}}>
                            <FormControlLabel label="Role" labelPlacement="top" style={{display: "flex"}} control={
                                <ToggleButtonGroup
                                    size="large"
                                    value={this.state.role}
                                    exclusive fullWidth
                                    onChange={(e, role) => this.setState({role})}
                                >
                                    <ToggleButton value="ca" disabled={! this.props.can_create_ca}>CA / Intermediate</ToggleButton>
                                    <ToggleButton value="server" disabled={! this.props.can_create_server}>Server</ToggleButton>
                                </ToggleButtonGroup>
                            }/>
                        </Grid>

                        <Grid item xs={12} lg={6} textAlign="center" style={{margin: "1rem 0"}}>
                            <input type="hidden" name="signature_algorithm" value={this.state.signatureAlgorithm}/>
                            <FormControlLabel label="Signature Algorithm" labelPlacement="top" style={{display: "flex"}} control={
                                <ToggleButtonGroup
                                    size="large"
                                    value={this.state.signatureAlgorithm}
                                    exclusive fullWidth
                                    onChange={(e, signatureAlgorithm) => this.setState({signatureAlgorithm})}
                                >
                                    <ToggleButton value="RSA">RSA</ToggleButton>
                                    <ToggleButton value="DSA">DSA</ToggleButton>
                                    <ToggleButton value="ECDSA">ECDSA</ToggleButton>
                                </ToggleButtonGroup>
                            }/> 
                        </Grid>
                        <Grid item xs={12} lg={6} textAlign="center" style={{margin: "1rem 0"}}>
                            {['RSA', 'DSA'].includes(this.state.signatureAlgorithm) && <TextField
                                fullWidth
                                margin="normal"
                                label="Key length"
                                name="bits"
                                type="number"
                                defaultValue={2048}
                                inputProps={{min:2048}}
                                style={{marginTop: "1.5rem"}}
                            />}
                            {['ECDSA'].includes(this.state.signatureAlgorithm) && 
                                <FormControl fullWidth 
                                    style={{marginTop: "1.5rem"}}>
                                    <InputLabel id="curveNameLabel">Curve</InputLabel>
                                    <Select
                                        labelId="curveNameLabel"
                                        inputProps={{
                                            name: 'curve',
                                        }}
                                        label="Curve"
                                    >
                                        { this.curves?.["ecdsa"].map(curve => <MenuItem value={curve}>{curve}</MenuItem>) }
                                    </Select>
                                </FormControl>
                            }
                        </Grid>
                    </Grid>

                    <Grid container style={{display: this.state.activeStep === 1 ? "block" : "none"}}>
                        <Grid item xs={12} md={12}>
                            <DistinguishedNameField/>
                        </Grid>
                        <Grid item xs={12}>
                            <ValidityField/>
                        </Grid>
                    </Grid>

                    <Grid container style={{display: this.state.activeStep === 2 && this.state.role === 'server' ? "block" : "none"}}>
                        <Grid item xs={12}>
                            <SANField/>
                        </Grid>
                    </Grid>
                </form>
            </DialogContent>

            <DialogActions>
                {this.state.activeStep > 0 && <Button onClick={evt => this.setState({activeStep: this.state.activeStep-1})}>Prev</Button>}
                <div style={{flex: 2}}></div>
                <Button onClick={() => this.props.onCancel()}>Cancel</Button>
                {this.state.activeStep === lastStep && <Button type="submit" form="ca.create">Create</Button>}
                {this.state.activeStep < lastStep && <Button onClick={evt => this.setState({activeStep: this.state.activeStep+1})}>Next</Button>}
            </DialogActions>
        </Dialog>;
    }

    onSubmit(evt: FormEvent<HTMLFormElement>) {
        evt.preventDefault();
        const data = new FormData(evt.currentTarget);
        this.props.onSubmit(this.state.role, data);
        evt.currentTarget.reset();
    }
}