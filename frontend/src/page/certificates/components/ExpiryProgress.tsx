import React from "react";
import {Box, LinearProgress} from "@mui/material";

export class ExpiryProgress extends React.Component<Readonly<{
    valid_from: Date,
    valid_until: Date
}>> {
    get expirationProgress(): number {
        const from = new Date(this.props.valid_from).getTime();
        const until = new Date(this.props.valid_until).getTime();
        return (new Date().getTime() - from) / (until - from);
    }

    render() {
        return <Box sx={{ display: 'flex', alignItems: 'center', mt: 1, mr: 5 }}>
            <Box sx={{ flex: 0, textAlign: 'left' }}>{this.props.valid_from.toISOString().split('T')[0]}</Box>
            <Box sx={{ flex: 1, px: 2 }}><LinearProgress variant="determinate" value={this.expirationProgress}/></Box>
            <Box sx={{ flex: 0, textAlign: 'right' }}>{this.props.valid_until.toISOString().split('T')[0]}</Box>
        </Box>;
    }
}
