export type CaCertificate = {
    certificate_id: string,
    parent_certificate_id: string,
    distinguished_name: string,
    subject_alternative_names: string,
    key_usage: string,
    basic_constraints: string,
    valid_from: string,
    valid_until: string
};