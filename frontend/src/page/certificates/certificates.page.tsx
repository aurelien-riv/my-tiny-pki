import {Title} from "../../dashboard/Title";
import React from "react";
import {
    Button,
    Container,
    List,
} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import {CertificateEntry} from "./components/CertificateList";
import {CaCertificate} from "./components/_CaCertificateType";
import {CertificateRole, CreateCertificateFormDialog} from "./components/CreateCertificateForm";
import { BackendApi } from "../../service/BackendApi";

export class CertificatesPage extends React.Component<{}, {
    roots: Array<CaCertificate>,
    displayCreateForm: boolean
}> {
    constructor(props: {}) {
        super(props);
        this.state = {
            roots: [],
            displayCreateForm: false
        }
    }

    async componentDidMount() {
        Title.updateMainTitle("TLS server certificates");

        this.setState({
            roots: await (await BackendApi.fetchJson('/api/certificates'))
        });
    }

    showCreateForm() {
        this.setState({
            displayCreateForm: true
        });
    }

    closeCreateForm() {
        this.setState({
            displayCreateForm: false
        });
    }

    async createCaAndClose(role: CertificateRole, formdata: FormData) {
        this.closeCreateForm();

        if (role !== 'ca') {
            throw 'Only self-signed CA are currently supported';
        }

        let payload = await BackendApi.fetchJson('/api/ca', {
            method: 'POST',
            body: JSON.stringify({
                distinguished_name: formdata.get("distinguished_name"),
                days: parseInt(formdata.get("days") as string),
                algorithm: {
                    [formdata.get("signature_algorithm") as string]: {
                        bits: parseInt(formdata.get("bits") as string),
                        curve: formdata.get("curve")
                    }
                }
            })
        });
        this.setState({
            roots: payload
        });
    }

    render() {
        return <Container component="main" maxWidth="xl">
            <Button startIcon={<AddIcon/>} onClick={this.showCreateForm.bind(this)}>Create a root certificate</Button>
            <CreateCertificateFormDialog
                can_create_ca={true}
                can_create_server={false}
                open={this.state.displayCreateForm}
                onSubmit={this.createCaAndClose.bind(this)}
                onCancel={this.closeCreateForm.bind(this)}
            />
            <List sx={{ width: '100%' }}>
                {this.state.roots.map(c =>
                    <CertificateEntry
                        key={c.certificate_id}
                        certificate={c}
                        onDelete={this.onChildDeleted.bind(this, c.certificate_id)}
                    />
                )}
            </List>
        </Container>;
    }

    onChildDeleted(certificate_id: string) {
        this.setState({
            roots: this.state.roots?.filter(c => c.certificate_id !== certificate_id)
        })
    }
}