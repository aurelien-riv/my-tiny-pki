import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import auth from "../service/auth";
import {BackendApi} from "../service/BackendApi";

type LoginState = {
    error?: string|null
}
export class Login extends React.Component<Readonly<{}>, LoginState> {
    handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        BackendApi.fetchAllowingErrors(`/api/auth/login`, {
            method: "POST",
            body: JSON.stringify({
                email: data.get('email'),
                password: data.get('password')
            })
        }).then(async payload => {
            if (payload.ok) {
                auth.setNewToken(await payload.json())
                    .then(() => window.location.replace('/'))
                    .catch(() => {});
            } else {
                this.setState({
                    error: "Invalid login or password, or inactive account"
                });
            }
        }).catch(() => {});
    };

    render() {
        return (
            <Grid container component="main" sx={{ height: '100vh' }}>
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: 'url(https://theme.bordeaux-pipeband.fr/images/tartan.jpg)',
                        backgroundRepeat: 'no-repeat',
                        backgroundColor: (t) =>t.palette.grey[50],
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <Box component="form" noValidate onSubmit={this.handleSubmit.bind(this)} sx={{ mt: 1 }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                type="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                error={typeof this.state?.error === "string"}
                                helperText={this.state?.error}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Sign In
                            </Button>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        );
    }
}