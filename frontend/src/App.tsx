import Dashboard from './dashboard/Dashboard';
import * as React from 'react';
import { Route, Switch } from "react-router-dom";
import {CertificatesPage} from "./page/certificates/certificates.page";

export default class extends React.Component {
    render() {
      return <Dashboard
          content={
            <Switch>
              <Route path="/" component={CertificatesPage} />
            </Switch>
          }
        />
    }
}

