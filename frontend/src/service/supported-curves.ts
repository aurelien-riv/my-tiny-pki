import { BackendApi } from "./BackendApi";

class SupportedCurves {
    curves: Promise<Record<string, string[]>>;

    constructor() {
        this.curves = this.load();
    }

    async load() {
        return await BackendApi.fetchJson('/api/certificates/available_curves', {
            method: 'GET'
        });
    }
}

export const supportedCurves = new SupportedCurves();