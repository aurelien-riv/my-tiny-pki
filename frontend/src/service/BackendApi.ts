import SnackbarHelper from "../common/SnackBarManager";
import auth from "./auth";

export class BackendApi {
    static async fetchAllowingErrors(input: RequestInfo, init: RequestInit={}): Promise<Response> {
        try {
            return await fetch(input, init);
        } catch (e) {
            SnackbarHelper.error('Network error');
        }
        throw Error();
    }

    static async fetchJson(input: RequestInfo, init: RequestInit={}): Promise<any> {
        const data = await BackendApi.fetch(input, init);

        try {
            return await data.json();
        } catch (e) {
            SnackbarHelper.error('Invalid value received');
        }
        throw Error();
    }

    static async fetch(input: RequestInfo, init: RequestInit={}): Promise<any> {
        try {
            if (auth.isLoggedIn) {
                init.headers = {
                    ...init.headers,
                    'Authorization': `Bearer ${auth.raw_token}`
                };
            }
            console.warn(init.headers);
            const data = await fetch(input, init);
            if (data.ok) {
                return data;
            } else {
                SnackbarHelper.error('Something went wrong');
            }
        } catch (e) {
            SnackbarHelper.error('Network error');
        }
        throw Error();
    }
}