import {decodeToken, isExpired} from "react-jwt";

type AuthTokenType = {
    user_id: number
};

class Auth {
    public raw_token: string | null;
    public token: AuthTokenType | null;

    constructor() {
        this.token = null;

        this.raw_token = sessionStorage.getItem("jwt_token");
        if (this.raw_token) {
            if (! isExpired(this.raw_token)) {
                this.token = decodeToken(this.raw_token) as AuthTokenType;
            }
        }
    }

    async setNewToken(token: string) {
        this.raw_token = token;
        this.token = decodeToken(token) as AuthTokenType;
        sessionStorage.setItem("jwt_token", token);
    }

    forgetToken() {
        sessionStorage.removeItem("jwt_token");
        this.raw_token = null;
        this.token = null;
    }

    get isLoggedIn() {
        return this.token !== null;
    }
}

export default new Auth();