import Typography from "@mui/material/Typography";
import * as React from "react";

export class Title extends React.Component {
    static updateMainTitle(newTitle: string) {
        document.getElementById("main-title")!.innerHTML = newTitle;
    }

    render() {
        return <Typography
            id="main-title"
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
            sx={{flexGrow: 1}}
        >
            Dashboard
        </Typography>;
    }
}