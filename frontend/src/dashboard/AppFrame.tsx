import * as React from "react";
import {AppBar, Avatar, Chip, Toolbar} from "@mui/material";
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import auth from "../service/auth";
import {Title} from "./Title";
import { BackendApi } from "../service/BackendApi";

export class AppFrame extends React.Component<Readonly<{}>, {}> {
    render() {
        return <AppBar position="absolute">
            <Toolbar sx={{pr: '24px'}}>
                <Title/>
                <Chip
                    sx={{
                        "& > .MuiChip-label": {paddingLeft: 0},
                        "& > .MuiChip-deleteIcon": {color: "#bdbdbd"}
                    }}
                    variant="outlined"
                    deleteIcon={<PowerSettingsNewIcon/>}
                    onDelete={this.onClickLogout.bind(this)}
                />
            </Toolbar>
        </AppBar>;
    }

    onClickLogout() {
        auth.forgetToken();
        window.location.replace('/');
    };
}