import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import {Container, Paper} from "@mui/material";
import {AppFrame} from "./AppFrame";

type DashboardContentProps = {
    content: React.ReactNode
}
function DashboardContent(props: DashboardContentProps) {
    return  <Box sx={{
            display: 'flex'
        }}>
            <CssBaseline/>
            <AppFrame/>
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) => theme.palette.grey[100],
                    flexGrow: 1,
                    height: '100vh',
                    overflow: 'auto',
                }}
            >
                <Container maxWidth="xl" sx={{mt: 10, mb: 4}}>
                    {props.content}
                </Container>
            </Box>
        </Box>;
}

export default function Dashboard(props: DashboardContentProps) {
    return <DashboardContent content={props.content}/>;
}
