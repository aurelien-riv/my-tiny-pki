const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "bundle.[chunkhash].js",
        path: path.resolve(__dirname, "dist"),
        hashFunction: "xxhash64"
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./public/index.html",
            publicPath: '/'
        }),
    ],
    resolve: {
        modules: [__dirname, "src", "node_modules"],
        extensions: ["*", ".js", ".jsx", ".tsx", ".ts"],
    },
    module: {
        rules: [
            {
                test: /\.[jt]sx?$/,
                exclude: /node_modules/,
                loader: require.resolve("babel-loader"),
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            }
        ],
    },
    devServer: {
        historyApiFallback: true,
        proxy: {
            '/api': {
              target: 'https://localhost:8001',
              secure: false
            }
        },
    },
};
